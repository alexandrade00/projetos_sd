package utils;

import java.io.File;
import java.util.List;

public class SyncUtils {

    public void createMissingDirectories(String file, String currentDir) {

        String[] directories = file.split("/");
        StringBuilder sb = new StringBuilder();
        sb.append(currentDir);

        for (int i = 0; i < directories.length - 1; i++) {
            sb.append(directories[i] + "/");
            File tempFile = new File(sb.toString());
            System.out.println(sb.toString());

            if (!tempFile.exists())
                tempFile.mkdir();
        }
    }

    public void getDirectoryTree(File folder, List<String> fileList, StringBuilder sb) {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) {
                StringBuilder sb1 = new StringBuilder();
                sb1.append(sb);
                sb1.append(file.getName() + "/");
                getDirectoryTree(file, fileList, sb1);
            } else {
                fileList.add(sb.toString() + file.getName());
            }
        }
    }
}
