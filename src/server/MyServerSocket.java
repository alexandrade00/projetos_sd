import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import utils.SyncUtils;


class MyServerSocket {
    static LogInUsers loggedUsers = new LogInUsers();
    private ServerSocket listenSocket;
    public static String SERVER_DIR;
    public static final String CONFIG_DIR = "config/";
    public static final String USERS_DIR = "config/users.csv";

    public MyServerSocket(int port, String serverDir) {
        try {
            MyServerSocket.SERVER_DIR = serverDir;
            this.listenSocket = new ServerSocket(port);
        } catch (IOException e) {
            System.out.printf("Error: %s\n", e.getMessage());
        }
    }

    public void acceptNewConnections() {
        try {
            Socket client = this.listenSocket.accept();
            new MyClient(client);

        } catch (IOException e) {
            System.out.printf("Can't create object Client - error: %s\n", e.getMessage());
        }
    }

    public void closeSocket() {
        try {
            listenSocket.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}

class MyClient implements Runnable {
    private Socket client;
    private DataInputStream in;
    private DataOutputStream out;
    private final AtomicBoolean running = new AtomicBoolean(false);
    private static final SecureRandom secureRandom = new SecureRandom(); // threadsafe
    private static final Base64.Encoder base64Encoder = Base64.getUrlEncoder(); // threadsafe

    public MyClient(Socket aClient) {
        try {
            this.client = aClient;
            System.out.println(this.client);
            this.in = new DataInputStream(this.client.getInputStream());
            this.out = new DataOutputStream(this.client.getOutputStream());
            new Thread(this).start();
        } catch (IOException e) {
            System.out.printf("Can't initialize thread - error: %s\n", e.getMessage());
        }
    }

    public void run() {
        running.set(true);
        try {
            while (running.get()) {

                String[] request = this.in.readUTF().split(" ");
                System.out.println(request[0]);
                switch (request[0]) {

                    case "login:":
                        this.out.writeUTF(login(request[1], request[2]));
                        break;

                    // case "register:":
                    // this.out.writeUTF(register(request[1], request[2]));
                    // break;

                    case "logout:":
                        if (MyServerSocket.loggedUsers.logOutUser(request[1]))
                            this.out.writeUTF("You are logged out!");
                        else
                            this.out.writeUTF("You are not logged in!");
                        break;

                    case "password:":
                        this.out.writeUTF(changePassword(request[1], request[2]));
                        break;

                    case "ls":
                        this.out.writeUTF(listFiles(request[1]));
                        break;

                    case "cd":
                        this.out.writeUTF(changeDir(request[2], request[1]));
                        break;

                    case "rvf":
                        sendFile(request[1], request[2]);
                        break;

                    case "sf":
                        receiveFile(request[1], request[2]);
                        break;

                    default:
                        System.out.println("Client disconnected");
                        this.stop();
                }
            }
        } catch (IOException e) {
            System.out.println("Client disconnected " + this.client);
        }
    }

    private String login(String username, String password) {
        if (MyServerSocket.loggedUsers.isUserLogged(username))
            return "You are already logged in!";

        Scanner f_sc;
        try {
            f_sc = new Scanner(new File(MyServerSocket.USERS_DIR));
            f_sc.useDelimiter("\n");
            while (f_sc.hasNext()) {
                String[] splitted_info = f_sc.next().split(", ");
                if (splitted_info[0].equals(username) && splitted_info[1].equals(password)) {
                    f_sc.close();
                    String authToken = generateNewToken();
                    MyServerSocket.loggedUsers.addUser(username, authToken);
                    return "T " + authToken;
                }
            }
        } catch (FileNotFoundException e) {
            System.out.printf("Error: %s\n", e.getMessage());
        }
        return "Wrong username or password";
    }

    private String changePassword(String token, String newPassword) throws IOException {
        if (MyServerSocket.loggedUsers.isUserLogged(token)) {
            Scanner f_sc;
            BufferedWriter bw = null;
            bw = new BufferedWriter(new FileWriter(MyServerSocket.CONFIG_DIR + "usersTemp.csv"));

            try {
                f_sc = new Scanner(new File(MyServerSocket.USERS_DIR));
                f_sc.useDelimiter("\n");
                while (f_sc.hasNext()) {
                    String[] splitted_info = f_sc.next().split(", ");
                    if (splitted_info[0].equals(MyServerSocket.loggedUsers.getUsername(token)) == false) {
                        String str = String.format("%s, %s\n", splitted_info[0], splitted_info[1]).toString();
                        bw.write(str);
                    } else {
                        String str = String
                                .format("%s, %s\n", MyServerSocket.loggedUsers.getUsername(token), newPassword)
                                .toString();
                        bw.write(str);
                    }

                }
                bw.close();
                f_sc.close();

                File antigo = new File(MyServerSocket.USERS_DIR);
                antigo.delete();

                File novo = new File(MyServerSocket.CONFIG_DIR + "usersTemp.csv");
                novo.renameTo(antigo);
            } catch (FileNotFoundException e) {
                System.out.printf("Error: %s\n", e.getMessage());
            }
        }
        return "Password changed!";
    }

    private String listFiles(String token) {

        if (MyServerSocket.loggedUsers.isUserLogged(token)) {
            String dir = MyServerSocket.loggedUsers.getCurrentDir(token);

            File currDir = new File(MyServerSocket.SERVER_DIR + dir);
            try {
                return String.join("\n", currDir.list());
            } catch (NullPointerException e) {
                return "No File Found";
            }
        }

        return "You are not logged in";
    }

    private String changeDir(String token, String dir) {

        if (MyServerSocket.loggedUsers.isUserLogged(token)) {
            String myDir = MyServerSocket.loggedUsers.getCurrentDir(token);
            File currDir = new File(MyServerSocket.SERVER_DIR + myDir);

            if (dir.equals("..") && !currDir.getParentFile().getName().equals("MainServer")) {
                System.out.println(currDir.getParentFile().getName());
                MyServerSocket.loggedUsers.updateCurrentDir(token, currDir.getParentFile().getName() + "/");
                return currDir.getParentFile().getName();
            } else if (Arrays.asList(currDir.list()).contains(dir)) {
                String newDir = myDir + "/" + dir;
                MyServerSocket.loggedUsers.updateCurrentDir(token, newDir);
                return newDir;
            }
            return "false";
        }
        return "false";
    }

    private void receiveFile(String token, String filePath) throws UnknownHostException, IOException {
        if (!MyServerSocket.loggedUsers.isUserLogged(token))
            return;

        new FileTransferHandler(this.out, false, filePath);
    }

    private void sendFile(String token, String filePath) throws UnknownHostException, IOException {
        if (!MyServerSocket.loggedUsers.isUserLogged(token))
            return;

        new FileTransferHandler(this.out, true, filePath);
    }

    public void stop() {
        this.running.set(false);
    }

    private static String generateNewToken() {
        byte[] randomBytes = new byte[24];
        secureRandom.nextBytes(randomBytes);
        return base64Encoder.encodeToString(randomBytes);
    }
}

class LogInUsers {
    private List<String> authTokens;
    private List<String> usernames;
    private List<String> currentDir;

    public LogInUsers() {
        usernames = new ArrayList<String>();
        currentDir = new ArrayList<String>();
        authTokens = new ArrayList<String>();
    }

    public void addUser(String username, String token) {
        usernames.add(username);
        currentDir.add("home_" + username);
        authTokens.add(token);
    }

    public boolean logOutUser(String token) {
        int index = authTokens.indexOf(token);

        if (index == -1)
            return false;

        usernames.remove(index);
        currentDir.remove(index);
        authTokens.remove(index);
        return true;
    }

    public boolean isUserLogged(String token) {
        return authTokens.indexOf(token) != -1 ? true : false;
    }

    public String getCurrentDir(String token) {
        return currentDir.get(authTokens.indexOf(token));
    }

    public String getUsername(String token) {
        return usernames.get(authTokens.indexOf(token));
    }

    public void updateCurrentDir(String token, String newDir) {
        this.currentDir.set(authTokens.indexOf(token), newDir);
    }

}

class FileTransferHandler implements Runnable {
    private Socket transferSocket;
    private boolean flag;
    private String filePath;
    private DataOutputStream comSocketOut;
    // send - true
    // receive - false

    public FileTransferHandler(DataOutputStream comSocketOut, boolean sendOrReceive, String filePath) {
        try {
            ServerSocket sendSocket = new ServerSocket(0);
            comSocketOut.writeUTF(Integer.toString(sendSocket.getLocalPort()));
            this.transferSocket = sendSocket.accept();
            this.flag = sendOrReceive;
            this.filePath = filePath;
            this.comSocketOut = comSocketOut;
            new Thread(this).start();
            sendSocket.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void run() {

        if (this.flag == true) {
            File file = new File(MyServerSocket.SERVER_DIR + this.filePath);

            try {
                FileInputStream fis = new FileInputStream(file);
                BufferedInputStream bis = new BufferedInputStream(fis);
                DataOutputStream ot = new DataOutputStream(transferSocket.getOutputStream());
                byte[] contents;
                long fileLength = file.length();
                long current = 0;
                long start = System.nanoTime();

                while (current != fileLength) {
                    int size = 10000;
                    if (fileLength - current >= size)
                        current += size;
                    else {
                        size = (int) (fileLength - current);
                        current = fileLength;
                    }
                    contents = new byte[size];
                    bis.read(contents, 0, size);
                    ot.write(contents);
                    System.out.print("Sending file ... " + (current * 100) / fileLength + "% complete!");
                }
                System.out.printf("File sent in %d!\n", System.nanoTime() - start);
                ot.flush();
                bis.close();
                this.comSocketOut.writeUTF("Success!");

            } catch (FileNotFoundException e) {

                try {
                    DataOutputStream ot = new DataOutputStream(transferSocket.getOutputStream());
                    ot.write(-1);
                    this.comSocketOut.writeUTF("No such file!");
                } catch (IOException e2) {
                    System.out.println("File does not exist");
                }

            } catch (IOException e1) {
                System.out.println(e1.getMessage());
            }

        } else {
            File file = new File(MyServerSocket.SERVER_DIR + this.filePath);

            try {
                byte[] fileParts = new byte[10000];
                FileOutputStream fos = new FileOutputStream(file);
                BufferedOutputStream bos = new BufferedOutputStream(fos);
                InputStream is = this.transferSocket.getInputStream();
                int bytesRead = 0;
                while ((bytesRead = is.read(fileParts)) != -1)
                    bos.write(fileParts, 0, bytesRead);

                System.out.println("File Received!");
                bos.flush();
                bos.close();
                this.transferSocket.close();
            } catch (FileNotFoundException e1) {
                System.out.println(e1.getMessage());
            } catch (IOException e2) {
                System.out.println(e2.getMessage());
            }
        }

        try {
            this.transferSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class SendHeartBeats {
    private static final long DELAY = 1000L;
    private static final long PERIOD = 1000L;
    private ScheduledExecutorService executor;

    public SendHeartBeats(String ip, int port, String payload) {
        executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(new PayloadUdpSender(ip, port, payload), DELAY, PERIOD, TimeUnit.MILLISECONDS);
    }
}

class PayloadUdpSender implements Runnable {
    private DatagramSocket heartbeatSocket;
    private DatagramPacket sendPacket;
    private static final Logger LOGGER = Logger.getLogger(PayloadUdpSender.class.getName());

    public PayloadUdpSender(String ip, int port, String payload) {
        try {
            heartbeatSocket = new DatagramSocket();
            byte[] sendData = payload.getBytes();
            sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getByName(ip), port);
        } catch (SocketException exc) {
            LOGGER.log(Level.SEVERE, "Socket error\n" + exc.getMessage());
        } catch (UnknownHostException exc) {
            LOGGER.log(Level.SEVERE, "Unknown host error\n" + exc.getMessage());
        }
    }

    @Override
    public void run() {
        try {
            heartbeatSocket.send(sendPacket);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "IO error\n" + e.getMessage());
        }
    }
}

class DirectoriesSender implements Runnable {
    private SyncUtils fileUtils;
    private DatagramSocket aSocket;
    private DatagramPacket reply;
    public static int FILE_TRANSFER_UDP_PORT;

    public DirectoriesSender(int port, int file_port) {
        try {
            System.out.println("Synchronizer activated");
            FILE_TRANSFER_UDP_PORT = file_port;
            fileUtils = new SyncUtils();
            this.aSocket = new DatagramSocket(port);
            new Thread(this).start();
        } catch (SocketException e) {
            System.out.println("Socket: " + e.getMessage());
        }
    }

    @Override
    public void run() {
        try {
            while (true) {
                byte[] buffer = new byte[1000];
                DatagramPacket request = new DatagramPacket(buffer, buffer.length);
                aSocket.receive(request);
                String[] response = new String(request.getData(), 0, request.getLength()).split(" ");
                if (response[0].equals("send_directories")) {
                    System.out.println("Sending directories");

                    List<String> fileList = new ArrayList<String>();
                    StringBuilder sb = new StringBuilder();
                    fileUtils.getDirectoryTree(new File(MyServerSocket.SERVER_DIR), fileList, sb);
                    String directories = fileList.toString();

                    reply = new DatagramPacket(directories.getBytes(),
                            directories.getBytes().length, request.getAddress(), request.getPort());
                    aSocket.send(reply);
                } else if (response[0].equals("sending_file")) {
                    UDPFileTransfer sync = new UDPFileTransfer();
                    try {
                        sync.new FileReceiver(FILE_TRANSFER_UDP_PORT, MyServerSocket.SERVER_DIR);
                    } catch (IOException e1) {
                        System.out.println("Failed to Syncro from Backup Server");
                        fileUtils.createMissingDirectories(response[1], MyServerSocket.SERVER_DIR);
                    }
                } else if (response[0].equals("send_me_file")) {
                    UDPFileTransfer sync = new UDPFileTransfer();
                    try {
                        System.out.println(response[1]);
                        sync.new FileSender(FILE_TRANSFER_UDP_PORT, Server.IP,
                                MyServerSocket.SERVER_DIR + response[1],
                                response[1]);
                    } catch (IOException e1) {
                        e1.getStackTrace();
                        System.out.println("Failed to Syncro to Backup Server");
                    }
                } else {
                    String directories = "error";
                    reply = new DatagramPacket(directories.getBytes(),
                            directories.getBytes().length, request.getAddress(), request.getPort());
                    aSocket.send(reply);

                }
            }
        } catch (IOException e) {
            System.out.println("IO: " + e.getMessage());
        }

    }
}
