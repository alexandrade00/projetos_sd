import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class PrimaryAlive implements Runnable {
    DatagramSocket sendingResponsSocket;
    private Thread responseGiver;

    public PrimaryAlive(int port) {
        try {
            sendingResponsSocket = new DatagramSocket(port);
            responseGiver = new Thread(this);
            responseGiver.start();
        } catch (SocketException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
        try {
            while (!responseGiver.isInterrupted()) {
                String message = "Who Are You?";
                byte[] questionFromSomeone = new byte[1024];

                DatagramPacket receivedPacket = new DatagramPacket(questionFromSomeone, questionFromSomeone.length);
                sendingResponsSocket.receive(receivedPacket);

                if (new String(receivedPacket.getData(), 0, receivedPacket.getLength()).equals("UCDrive")) {
                    message = "yes";
                }

                byte[] messageBytes = message.getBytes();
                DatagramPacket msgToMain = new DatagramPacket(messageBytes, messageBytes.length,
                        receivedPacket.getAddress(), receivedPacket.getPort());

                sendingResponsSocket.send(msgToMain);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        this.responseGiver.interrupt();
    }
}
