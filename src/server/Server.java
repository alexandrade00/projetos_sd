import utils.SyncUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import heartbeat.HeartbeatEvent;
import heartbeat.HeartbeatListener;
import heartbeat.HeartbeatProtocolManager;

public class Server implements HeartbeatListener {
    static acceptConsBackup server;
    static SyncExecutor myExec;
    static HeartbeatProtocolManager heartbeatProtocolManager;

    public static String IP;
    public static String DIR_MAIN_SERVER;
    public static String DIR_BACKUP_SERVER;
    public static int FILE_TRANSFER_UDP_PORT;
    public static int MAIN_SERVER_TCP_PORT;
    public static int SYNC_UDP_PORT;
    public static int HEARTBEAT_UDP_PORT;
    public static int CHECK_SERVER_UP_UDP_PORT;
    public static final String SERVER_INFO = "config/server_ports.csv";

    private static final Logger LOGGER = Logger.getLogger(Server.class.getName());

    public static void main(String args[]) {
        readConfigFile();
        if (availablePort(CHECK_SERVER_UP_UDP_PORT)) {
            System.out.println("Im Primary");
            new SendHeartBeats(IP, HEARTBEAT_UDP_PORT, "MainServer");
            new DirectoriesSender(SYNC_UDP_PORT, FILE_TRANSFER_UDP_PORT);
            new PrimaryAlive(CHECK_SERVER_UP_UDP_PORT);
            server = new acceptConsBackup(MAIN_SERVER_TCP_PORT, DIR_MAIN_SERVER);
        } else {
            System.out.println("Im Secondary");
            heartbeatProtocolManager = new HeartbeatProtocolManager(new Server());
            heartbeatProtocolManager.addHost("MainServer", HEARTBEAT_UDP_PORT);
            myExec = new SyncExecutor(SYNC_UDP_PORT);
        }
    }

    @Override
    public void onHeartbeat(HeartbeatEvent event) {
        LOGGER.log(Level.INFO, () -> "Received heartbeat from " + event.getSource() + " in "
                + event.getTimeFromPreviousBeat() + " at " + event.getBeatTimeStamp());
    }

    @Override
    public void onDeath(HeartbeatEvent event) {
        LOGGER.log(Level.INFO, () -> event.getSource() + " died after " + event.getTimeFromPreviousBeat() + " at "
                + event.getBeatTimeStamp());

        myExec.killExec();
        heartbeatProtocolManager.removeFromMonitoredHost("MainServer");
        heartbeatProtocolManager.kill();
        new SendHeartBeats(IP, HEARTBEAT_UDP_PORT, "MainServer");
        new DirectoriesSender(SYNC_UDP_PORT, FILE_TRANSFER_UDP_PORT);
        new PrimaryAlive(CHECK_SERVER_UP_UDP_PORT);
        server = new acceptConsBackup(MAIN_SERVER_TCP_PORT, DIR_MAIN_SERVER);
    }

    @Override
    public void onLossCommunication(HeartbeatEvent event) {
        LOGGER.log(Level.INFO, () -> "Communication lost of " + event.getSource() + " in "
                + event.getTimeFromPreviousBeat() + " at " + event.getBeatTimeStamp());
    }

    @Override
    public void onReacquiredCommunication(HeartbeatEvent event) {
        LOGGER.log(Level.INFO, () -> "Communication reacquired of " + event.getSource() + " in "
                + event.getTimeFromPreviousBeat() + " at " + event.getBeatTimeStamp());
    }

    @Override
    public void onAcquiredCommunication(HeartbeatEvent event) {
        LOGGER.log(Level.INFO, () -> event.getSource() + " connected at " + event.getBeatTimeStamp());
    }

    private static boolean availablePort(int port) {
        try (DatagramSocket UDPSocket = new DatagramSocket()) {
            UDPSocket.setSoTimeout(1000);
            String message = "UCDrive";
            byte[] fileNameBytes = message.getBytes();
            byte[] answerFromMain = new byte[1024];

            DatagramPacket msgToMain = new DatagramPacket(fileNameBytes, fileNameBytes.length,
                    InetAddress.getLocalHost(), port);

            DatagramPacket receivedPacket = new DatagramPacket(answerFromMain, answerFromMain.length);

            UDPSocket.send(msgToMain);

            UDPSocket.receive(receivedPacket);

            if (new String(receivedPacket.getData(), 0, receivedPacket.getLength()).equals("yes")) {
                return false;
            }

        } catch (SocketTimeoutException e) {
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }

    private static void readConfigFile() {
        Scanner f_sc;
        try {
            f_sc = new Scanner(new File(SERVER_INFO));
            f_sc.useDelimiter("\n");
            while (f_sc.hasNext()) {
                String[] splitted_info = f_sc.next().split(", ");
                switch (splitted_info[0]) {
                    case "main server directory":
                        DIR_MAIN_SERVER = splitted_info[1];
                        break;
                    case "backup server directory":
                        DIR_BACKUP_SERVER = splitted_info[1];
                        break;
                    case "file transfer udp port":
                        FILE_TRANSFER_UDP_PORT = Integer.parseInt(splitted_info[1]);
                        break;
                    case "server port tcp":
                        MAIN_SERVER_TCP_PORT = Integer.parseInt(splitted_info[1]);
                        break;
                    case "heartbeat udp port":
                        HEARTBEAT_UDP_PORT = Integer.parseInt(splitted_info[1]);
                        break;
                    case "sync udp port":
                        SYNC_UDP_PORT = Integer.parseInt(splitted_info[1]);
                        break;
                    case "check server udp port":
                        CHECK_SERVER_UP_UDP_PORT = Integer.parseInt(splitted_info[1]);
                        break;
                    case "ip":
                        IP = splitted_info[1];
                        break;
                }

            }
        } catch (FileNotFoundException e) {
            System.out.printf("Error: %s\n", e.getMessage());
        }
    }
}

class acceptConsBackup implements Runnable {
    MyServerSocket server;
    private Thread connsBackup;

    public acceptConsBackup(int port, String serverDir) {
        server = new MyServerSocket(port, serverDir);
        connsBackup = new Thread(this);
        connsBackup.start();
    }

    @Override
    public void run() {
        while (!connsBackup.isInterrupted()) {
            server.acceptNewConnections();
        }
        // System.out.println("fui embora");
        server.closeSocket();
    }

    public void stop() {
        this.connsBackup.interrupt();
    }
}

class SyncExecutor {
    private static final long DELAY = 1L;
    private static final long PERIOD = 1L;
    private ScheduledExecutorService executor;

    public SyncExecutor(int port) {
        executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(new waitingSyncFiles(port), DELAY, PERIOD, TimeUnit.MINUTES);
    }

    public void killExec() {
        executor.shutdown();
    }
}

class waitingSyncFiles implements Runnable {
    private Thread connsBackup;
    private DatagramSocket aSocket;
    private SyncUtils fileUtils;
    int port;

    public waitingSyncFiles(int port) {
        try {
            this.port = port;
            aSocket = new DatagramSocket();
            fileUtils = new SyncUtils();
            connsBackup = new Thread(this);
            connsBackup.start();
        } catch (SocketException e) {
            System.out.println("Socket: " + e.getMessage());
        }
    }

    @Override
    public void run() {
        try {
            String texto = "send_directories";
            byte[] m = texto.getBytes();

            InetAddress aHost = InetAddress.getByName(Server.IP);
            DatagramPacket request = new DatagramPacket(m, m.length, aHost, port);
            aSocket.send(request);

            byte[] buffer = new byte[1000];
            aSocket.setSoTimeout(10000);
            DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
            aSocket.receive(reply);
            String mainDir = new String(reply.getData(), 0, reply.getLength());

            mainDir = mainDir.substring(1, mainDir.length() - 1);
            List<String> fileListMain = new ArrayList<String>(Arrays.asList(mainDir.split(", ")));

            List<String> fileList = new ArrayList<String>();
            StringBuilder sb = new StringBuilder();
            fileUtils.getDirectoryTree(new File(Server.DIR_BACKUP_SERVER), fileList, sb);

            List<String> differencesBackup = fileList.stream()
                    .filter(element -> !fileListMain.contains(element))
                    .collect(Collectors.toList());

            List<String> differencesMain = fileListMain.stream()
                    .filter(element -> !fileList.contains(element))
                    .collect(Collectors.toList());

            System.out.println(fileListMain);
            System.out.println(fileList);
            System.out.println(differencesBackup);

            fileSync(differencesBackup, differencesMain);
        } catch (SocketException e) {
            System.out.println("Sync Failed Socket: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Sync Failed IO: " + e.getMessage());
        }
    }

    private void fileSync(List<String> differencesBackup, List<String> differencesMain) {

        UDPFileTransfer sync = new UDPFileTransfer();

        try {
            for (String file : differencesBackup) {
                String texto = "sending_file " + file;
                byte[] m = texto.getBytes();
                DatagramPacket request = new DatagramPacket(m, m.length,
                        InetAddress.getByName(Server.IP), port);
                aSocket.send(request);
                try {
                    sync.new FileSender(Server.FILE_TRANSFER_UDP_PORT, Server.IP,
                            Server.DIR_BACKUP_SERVER + file,
                            file);
                } catch (IOException e) {
                    System.out.printf("Failed to send file: %s\n", file);
                }
                Thread.sleep(100);
            }

            for (String file : differencesMain) {
                String texto = "send_me_file " + file;
                byte[] m = texto.getBytes();
                DatagramPacket request = new DatagramPacket(m, m.length,
                        InetAddress.getByName(Server.IP), port);
                aSocket.send(request);
                try {
                    sync.new FileReceiver(Server.FILE_TRANSFER_UDP_PORT, Server.DIR_BACKUP_SERVER);
                } catch (IOException e) {
                    System.out.printf("Failed to receive file: %s\n", file);
                    fileUtils.createMissingDirectories(file, Server.DIR_BACKUP_SERVER);
                }
                Thread.sleep(100);
            }

        } catch (IOException | InterruptedException e1) {
            System.out.println("Error: " + e1.getMessage());
        }
    }

}