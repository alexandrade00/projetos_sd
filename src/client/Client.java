import java.io.*;
import java.net.*;
import java.util.*;
import java.util.Random;

public class Client {
    static ClientSocket client;
    public static boolean isLoggedIn = false;
    public static String currentDirectory;
    public static int MAIN_SERVER_TCP_PORT;
    public static String IP;

    public static void main(String args[]) {

        readConfigFile();
        client = new ClientSocket(IP, MAIN_SERVER_TCP_PORT);

        Random random = new Random();
        currentDirectory = "file_storage/Clients/"
                + Integer.toString(random.ints(1, 1, 1000).findFirst().getAsInt());
        System.out.println(currentDirectory);
        File myDir = new File(currentDirectory);
        myDir.mkdir();

        displayMenu(client);
    }

    private static void displayMenu(ClientSocket client) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int choosenOption;

        while (true) {
            if (!isLoggedIn)
                System.out.println(
                        "Choose an option:\n    1 - Login\n    2 - List Files\n    3 - Change Directory\n" +
                                "    4 - Change address and port\n    5 - Exit");
            else
                System.out.println(
                        "Choose an option:\n    1 - List Remote Files\n    2 - Change Remote Directory\n    3 - List Files\n"
                                +
                                "    4 - Change Directory\n    5 - Upload File\n    6 - Download File\n    7 - Change Password\n"
                                +
                                "    8 - Logout\n    0 - Exit");

            try {
                choosenOption = Integer.parseInt(br.readLine());

                if (!isLoggedIn) {
                    switch (choosenOption) {
                        case 1:
                            client.logIn();
                            break;
                        case 2:
                            client.listFiles();
                            break;
                        case 3:
                            client.changeDirectory();
                            break;
                        case 4:
                            client = client.changeSocketInfo();
                            break;
                        case 5:
                            client.logOut();
                            System.out.println("BYE BYE :D");
                            return;
                    }
                } else {
                    switch (choosenOption) {
                        case 1:
                            client.listRemoteFiles();
                            break;
                        case 2:
                            client.changeRemoteDirectory();
                            break;
                        case 3:
                            client.listFiles();
                            break;
                        case 4:
                            client.changeDirectory();
                            break;
                        case 5:
                            client.sendFile();
                            break;
                        case 6:
                            client.receiveFile();
                            break;
                        case 7:
                            client.passwordChange();
                            break;
                        case 8:
                            client.logOut();
                            break;
                        case 0:
                            client.logOut();
                            System.out.println("BYE BYE :D");
                            return;
                    }
                }
                System.out.println();
            } catch (IOException e) {
                System.out.printf("Error: %s\n", e.getMessage());
            } catch (NumberFormatException e1) {
                System.out.println("Insert only numbers!");
            }
        }
    }

    private static void readConfigFile() {
        Scanner f_sc;
        try {
            f_sc = new Scanner(new File("config/server_ports.csv"));
            f_sc.useDelimiter("\n");
            while (f_sc.hasNext()) {
                String[] splitted_info = f_sc.next().split(", ");
                if (splitted_info[0].equals("server port tcp")) {
                    MAIN_SERVER_TCP_PORT = Integer.parseInt(splitted_info[1]);
                } else if (splitted_info[0].equals("ip")) {
                    IP = splitted_info[1];
                }
            }
        } catch (FileNotFoundException e) {
            System.out.printf("Error: %s\n", e.getMessage());
        }
    }

}

class ClientSocket {
    private String whoIam;
    private String authToken;
    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;
    private String currentRemoteDirectory;

    public ClientSocket(String hostname, int port) {
        try {
            this.whoIam = new String("none");
            this.socket = new Socket(hostname, port);
            this.in = new DataInputStream(socket.getInputStream());
            this.out = new DataOutputStream(socket.getOutputStream());
            this.authToken = new String("none");
        } catch (UnknownHostException e) {
            System.out.printf("Error: %s\n", e.getMessage());
        } catch (IOException e) {
            System.out.printf("Error: %s\n", e.getMessage());
        }
    }

    public Socket getSocket() {
        return this.socket;
    }

    public DataInputStream getIn() {
        return this.in;
    }

    public DataOutputStream getOut() {
        return this.out;
    }

    public String getCurrentRemoteDirectory() {
        return this.currentRemoteDirectory;
    }

    public String getAuthToken() {
        return this.authToken;
    }

    // public void Register() {
    // try {
    // System.out.print("Insert user and pass (user pass): ");
    // BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    // String[] user_pass = br.readLine().split(" ");
    // this.out.writeUTF(String.format("register: %s %s", user_pass[0],
    // user_pass[1]));
    // String response = this.in.readUTF();
    // System.out.println(response);
    // this.whoIam = user_pass[0];
    // } catch (IOException e) {
    // System.out.printf("Error: %s\n", e.getMessage());
    // }
    // }

    public void logIn() {
        try {
            System.out.print("Insert user and pass (user pass): ");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String[] user_pass = br.readLine().split(" ");
            this.out.writeUTF(String.format("login: %s %s", user_pass[0], user_pass[1]));
            String response = this.in.readUTF();
            System.out.println(response);
            if (response.charAt(0) == 'T') {
                Client.isLoggedIn = true;
                this.authToken = response.split(" ")[1];
                this.whoIam = user_pass[0];
                this.currentRemoteDirectory = "home_" + whoIam;
            }
            System.out.println("You are logged in!");
        } catch (IOException e) {
            System.out.printf("Error: %s\n", e.getMessage());
        } catch (NullPointerException e1) {
            System.out.println("No Server found!");
        } catch (ArrayIndexOutOfBoundsException e2) {
            System.out.println("Wrong arguments");
        }
    }

    public void logOut() {
        try {
            this.out.writeUTF(String.format("logout: %s", this.authToken));
            String response = this.in.readUTF();
            System.out.println(response);
            this.whoIam = "none";
            Client.isLoggedIn = false;
        } catch (IOException e) {
            System.out.printf("Error: %s\n", e.getMessage());
        } catch (NullPointerException e1) {
            System.out.println("Not logged in!");
        }
    }

    public void passwordChange() {
        try {
            System.out.print("Insert new Password: ");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String new_pass = br.readLine();
            this.out.writeUTF(String.format("password: %s %s", this.authToken, new_pass));
            String response = this.in.readUTF();
            System.out.println(response);
        } catch (IOException e) {
            System.out.printf("Error: %s\n", e.getMessage());
        }
    }

    public void listRemoteFiles() {
        try {
            this.out.writeUTF("ls " + this.authToken);
            System.out.println("files in " + "\"" + currentRemoteDirectory + "\" directory:");
            String response = this.in.readUTF();
            System.out.println(response);
        } catch (IOException e) {
            System.out.printf("Error: %s\n", e.getMessage());
        }
    }

    public ClientSocket changeSocketInfo() {
        try {
            System.out.print("Insert address and port: ");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String[] input = br.readLine().split(" ");
            ClientSocket client = new ClientSocket(input[0], Integer.parseInt(input[1]));
            System.out.println("Socket alterado");
            return client;
        } catch (IOException e) {
            System.out.println("Error: Server not found\n\n");
        } catch (ArrayIndexOutOfBoundsException e1) {
            System.out.println("Wrong arguments");
        }

        return null;
    }

    public void changeRemoteDirectory() {
        try {
            System.out.print("Insert directory: ");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            this.out.writeUTF("cd " + br.readLine() + " " + authToken);
            String response = this.in.readUTF();
            if (!response.equals("false")) {
                this.currentRemoteDirectory = response;
                System.out.println("New directory: \"" + this.currentRemoteDirectory + "\"");
            } else
                System.out.println("Directory not found");

        } catch (IOException e) {
            System.out.println("Error: Server not found\n\n");
        }
    }

    public void changeDirectory() {
        try {
            System.out.print("Insert directory: ");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String wantedDir = br.readLine();
            String myDir = Client.currentDirectory;
            File currDir = new File(myDir);
            if (wantedDir.equals("..")) {
                Client.currentDirectory = currDir.getParentFile().getAbsolutePath();
                System.out.println(Client.currentDirectory);
                return;
            } else if (Arrays.asList(currDir.list()).contains(wantedDir)) {
                Client.currentDirectory = myDir + "/" + wantedDir;
                return;
            }
            System.out.println("Unknown directory!");

        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (NullPointerException e1) {
            System.out.println("Unknown directory!");
        }
    }

    public void receiveFile() throws UnknownHostException, IOException {
        System.out.print("Remote file path: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String filePath = br.readLine();
        new FileTransferHandlerClient(this, false, filePath);
    }

    public void sendFile() {
        try {
            System.out.print("File path: ");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String filePath = br.readLine();
            new FileTransferHandlerClient(this, true, filePath);
        } catch (IOException e1) {
            System.out.println(e1.getMessage());
        }

    }

    public void listFiles() {
        File currDir = new File(Client.currentDirectory);
        try {
            System.out.println("Files in " + currDir + ":");
            System.out.println(String.join("\n", currDir.list()));
        } catch (Exception e) {
            System.out.println("Invalid directory");
        }
    }

    // https://srikarthiks.files.wordpress.com/2019/07/file-transfer-using-tcp.pdf

}

class FileTransferHandlerClient implements Runnable {
    private boolean flag;
    private ClientSocket client;
    private String filePath;

    // send - true
    // receive - false

    public FileTransferHandlerClient(ClientSocket client, boolean flag, String filePath) {
        this.client = client;
        this.flag = flag;
        this.filePath = filePath;
        new Thread(this).start();
    }

    @Override
    public void run() {
        if (this.flag)
            sendFile();
        else
            receiveFile();

    }

    private void sendFile() {
        try {
            File file = new File(Client.currentDirectory + "/" + filePath);
            FileInputStream fis = new FileInputStream(file);
            System.out.println(this.client.getCurrentRemoteDirectory() + "/" + filePath);
            this.client.getOut()
                    .writeUTF(String.format("sf %s %s", this.client.getAuthToken(),
                            this.client.getCurrentRemoteDirectory() + "/" + filePath));
            int port = Integer.parseInt(this.client.getIn().readUTF());
            Socket TransferSocket = new Socket(Client.IP, port);
            BufferedInputStream bis = new BufferedInputStream(fis);
            OutputStream os = TransferSocket.getOutputStream();
            byte[] contents;
            long current = 0;
            long fileSize = file.length();
            long start = System.nanoTime();

            while (current != fileSize) {
                int size = 10000;
                if (fileSize - current >= size)
                    current += size;
                else {
                    size = (int) (fileSize - current);
                    current = fileSize;
                }
                contents = new byte[size];
                bis.read(contents, 0, size);
                os.write(contents);
                System.out.println("Sending file ... " + (current * 100) / fileSize + "% complete!");

            }
            os.flush();
            long end = System.nanoTime() - start;
            // File transfer done. Close the socket connection!
            bis.close();
            TransferSocket.close();
            System.out.printf("File sent succesfully in %d!\n", end);
        } catch (IOException e1) {
            System.out.println("File does not exist!");
        }
    }

    private void receiveFile() {
        if(filePath.equals(""))
            System.out.println("Invalid name!");
        else{
            try {
                this.client.getOut().writeUTF(String.format("rvf %s %s", this.client.getAuthToken(),
                        this.client.getCurrentRemoteDirectory() + "/" + filePath));
                int port = Integer.parseInt(this.client.getIn().readUTF());
                Socket ft_socket = new Socket(Client.IP, port);
                byte[] fileParts = new byte[10000];
                FileOutputStream fos = new FileOutputStream(Client.currentDirectory + "/" + filePath);
                BufferedOutputStream bos = new BufferedOutputStream(fos);
                InputStream is = ft_socket.getInputStream();
                int bytesRead = is.read(fileParts);
                System.out.println(bytesRead);
                System.out.println(fileParts[0]);

                if (fileParts[0] != -1) {

                    while (bytesRead != -1) {
                        bos.write(fileParts, 0, bytesRead);
                        bytesRead = is.read(fileParts);
                    }
                    bos.flush();
                } else {
                    bos.close();
                    fos.close();
                    ft_socket.close();
                    File emptyFile = new File(Client.currentDirectory + "/" + filePath);
                    emptyFile.delete();
                }

                System.out.println(this.client.getIn().readUTF());
                bos.close();
                fos.close();
                ft_socket.close();
            } catch (IOException e1) {
                System.out.println("No such file!");
                
            } catch (NumberFormatException e2) {
                System.out.println("No such file!");
            }
        }
    }
    
}